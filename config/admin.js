module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '3ac891390a3920f9ae326644145d612b'),
  },
});
